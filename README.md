Publishing tools for adacraft

# Build all static files

Command to build the adacraft portal and the Scratch Mod (the editor and the
player) for `main` branch:

```
$ ./src/build main
```

The static files will be put in `dist/main` directory.

The `build/main` directory is the working directory for this build step.

# Deploy

There are several deployment environments (staging, production...).

To deploy (for example into staging), place the files to deploy in
[deploy/current/staging](deploy/current/staging). Then execute the command:

```
$ ./src/deploy staging
```

This will upload the files to Bunny CDN and create a archive of the uploaded
files into [deploy/archive/staging](deploy/archive/staging) as a tar file named
with the deployment date.

The deployment date is also available online with the URL
https://staging.adacraft.org/deploy_date.txt (for the staging environment).
